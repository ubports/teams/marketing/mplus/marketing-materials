# Marketing Materials


## Brandbook

This reposity currently contains the UBports brandbook.
This brandbook defines the current styling of the project.
It is the result of the efforts of many individuals.
The initial version contained a lot of closed license content. This has been rectified as much as possible.

[Here you can find our brandbook](https://gitlab.com/ubports/teams/marketing/mplus/marketing-materials/-/blob/master/Brandbook/BrandBook-cc-1.0.pdf)





